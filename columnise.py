

import math
import os
import re
import shutil

try:
    from shutil import get_terminal_size  # Python >= 3.3
except ImportError:
    try:
        from backports.shutil_get_terminal_size import get_terminal_size
    except:  # noqa
        pass

DEFAULT_WIDTH = 80

def getDisplayWidth():
    try:
        width = int(os.environ["COLUMNS"])
    except (KeyError, ValueError):
        try:
            width = get_terminal_size().columns
        except:  # noqa
            width = DEFAULT_WIDTH

    return width or DEFAULT_WIDTH




def printcols(lst,  displaywidth=getDisplayWidth(), hsort=False, even=False,\
	colsep="  ", icolsep=" ", just="left", fill=" ", ifill=" ", lineprefix="", \
	linesuffix="", prefix="", suffix="", remove_tspace=True):

	if (size := len(lst)) == 0:
		return ""

	# Handle dicts
	if isinstance(lst, dict):
		lst2 = []
		for i in lst:
			lst2.append(f"{i} {lst[i]}")

		lst = lst2

	sf = stripformatting

	if isList := isinstance(lst[0], list):
		maxcols = min(5, size) + 1
	else:
		maxcols = min(40, size) + 1

	layouts = getLayouts(maxcols, size, even)
	# print(f"nlayouts: {len(layouts)}")

	# for layout in layouts:
	# 	print(layout)

	quotes = [ "'", '"', "\\"]

	name_prefix = ""
	quotey = False

	# Get size of prefix, but only after the last newline
	prefix_size = 0
	for c in prefix:
		prefix_size += 1
		if c == "\n":
			prefix_size = 0

	# Up to the first \n
	suffix_size = 0
	for c in suffix:
		if c == "\n":
			break
		suffix_size += 1

	col_sep_size = len(colsep)

	colwidths = []
	icolwidths = []

	array_index = lambda nrows, row, col: nrows * col + row

	if hsort:
		array_index = lambda ncols, row, col: ncols * row + col

	lineprefix_size = len(lineprefix)
	linesuffix_size = len(linesuffix)

	if isList:
		for layout in layouts:
			nrows = layout.rows
			ncols = layout.cols

			colwidths = []
			icolwidths = []
			line_width = -len(colsep)

			for col in range(ncols):
				colwidth = ColWidth()

				icols = []
				for v in lst[0]:
					icols.append(ColWidth())

				for row in range(nrows):
					i = array_index(nrows, row, col)
					if i >= size:
						break

					for icol in range(len(lst[i])):
						# plain = stripformatting(str(lst[i][icol]))
						plain = sf(str(lst[i][icol]))
						icolwidth = len(plain)

						if plain[0] in quotes:
							if not icols[icol].quote:
								icols[icol].quote = True
								icolwidth += 1
								quotey = True

						if icolwidth >= icols[icol].width:
							if plain[0] in quotes:
								icols[icol].qlong = True
							else:
								icols[icol].qlong = False
							icols[icol].width = icolwidth

					scolwidth = -len(icolsep)
					for j in range(len(lst[i])):
						scolwidth += icols[j].width + len(icolsep)

					colwidth.width = max(colwidth.width, scolwidth)

				if colwidth.quote:
					colwidth.width += 1

				if isList:
					icolwidths.append(icols)

				colwidths.append(colwidth)
				line_width += colwidth.width

			line_width += prefix_size + suffix_size + lineprefix_size + linesuffix_size + col_sep_size * (len(colwidths) - 1) #+ icol_sep_size * len()
			if line_width <= displaywidth:
				# print(line_width)
				print(displaywidth)
				break

	else:
		for layout in layouts:
			nrows = layout.rows
			ncols = layout.cols

			colwidths = []
			icolwidths = []
			line_width = -len(colsep)

			for col in range(ncols):
				colwidth = ColWidth()

				for row in range(nrows):
					i = array_index(nrows, row, col)
					if i >= size:
						break

					# plain = stripformatting(str(lst[i]))
					plain = sf(str(lst[i]))
					siz = len(plain)
					if plain[0] in quotes:
						colwidth.quote = True
						quotey = True

					if siz >= colwidth.width:
						if plain[0] in quotes:
							colwidth.qlong = True
						else:
							colwidth.qlong = False
						colwidth.width = siz

				if colwidth.quote:
					colwidth.width += 1

				if isList:
					icolwidths.append(icols)

				colwidths.append(colwidth)
				line_width += colwidth.width# + len(colsep)

			line_width += prefix_size + suffix_size + lineprefix_size + linesuffix_size + col_sep_size * (len(colwidths)) #+ icol_sep_size * len()
			if line_width <= displaywidth-3:
				# print(line_width)
				# print(displaywidth)
				break

	# if args.debug:
	# 	for icol in icolwidths:
	# 		print(icol)
	# 	print(colwidths)
	# 	print(ncols)

	bs = prefix
	# print(nrows, ncols)
	# print(icolwidths)
	# print(colwidths)

	if isList:
		for row in range(nrows):
			entries = []
			s = " " * prefix_size
			if row == 0:
				s = ""
			for col in range(ncols):
				i = array_index(nrows, row, col)
				if i >= size:
					break

				s2 = ""
				if col == 0:
					s2 = lineprefix

				ientries = []
				iplain = []
				for icol in range(len(lst[i])):
					s3 = ""
					x = lst[i][icol]
					plain = sf(x)
					extra = icolwidths[col][icol].width - len(plain)


					if icolwidths[col][icol].quote:
						if not plain[0] in quotes:
							if icolwidths[col][icol].qlong:
								extra -= 1
							s3 += " "
						else:
							if not icolwidths[col][icol].qlong:
								extra += 1

					sx = len(x)

					if just == "center":
						s3 += x.center(sx + extra, fill)
					elif just == "right":
						s3 += x.rjust(sx + extra, fill)
					else:
						s3 += x.ljust(sx + extra, fill)

					ientries.append(s3)

				x = icolsep.join(ientries)
				entries.append(x)
				entries.append(colsep)

			if remove_tspace and row == nrows -1:
				entries[-1] = entries[-1].rstrip()

			s += "".join(entries).rstrip() + linesuffix + "\n"

			bs += s

	else:
		for row in range(nrows):
			entries = []
			s = " " * prefix_size
			if row == 0:
				s = ""
			for col in range(ncols):
				i = array_index(nrows, row, col)
				if i >= size:
					break

				s2 = ""
				if col == 0:
					s2 = lineprefix
				x = lst[i]

				plain = sf(x)
				extra = colwidths[col].width - len(plain)
				eflag = False

				if colwidths[col].quote:
					if not plain[0] in quotes:
						extra -= 1
						if colwidths[col].qlong:
							extra -= 1
					else:
						if colwidths[col].qlong:
							extra -= 1
				if quotey:
					if plain[0] in quotes:
						name_prefix = ""
					else:
						name_prefix = " "

				sx = len(x)

				if just == "center":
					s2 += name_prefix + x.center(sx + extra, fill)
				elif just == "right":
					s2 += name_prefix + x.rjust(sx + extra, fill)
				else:
					s2 += name_prefix + x.ljust(sx + extra, fill) + colsep

				entries.append(s2)

			if remove_tspace and row == nrows -1:
				entries[-1] = entries[-1].rstrip()

			s += "".join(entries).rstrip() + linesuffix + "\n"

			bs += s

	print(bs.strip("\n").rstrip() + suffix)



class Layout:
	def __init__(self, rows, cols):
		self.rows = rows
		self.cols = cols
		self.plain = ""
		self.len_plain = 0
		# ecolws = []		# list of outer column widths
		# colws = []		# 2d list of colws for each entry column,

		# # for each outer column we have entries consisting of columns,
		# # we need a width for each of the nested columns
		# outer_col_widths = []
		# inner_col_widths = []


	def __repl__(self):
		return self.__str__()
		# return "<Layout rows:{} cols:{}>".format(self.rows, self.cols)

	def __str__(self):
		return "<Layout rows:{} cols:{}>".format(self.rows, self.cols)




class ColWidth:
	def __init__(self):
		self.width = 0
		self.quote = False

	def __str__(s):
		return f"<ColWidth: width = {s.width}, quote = {s.quote}>"

	def __repr__(s):
		return s.__str__()

def stripformatting(string):
	string = str(string)
	# print(string)
	# pat = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
	pat = re.compile(r'\x1B\[[0-9,;:]*?m')
	sub = pat.sub('', string)
	# print(sub)
	return sub


def getLayouts(maxcols, size, even):
	# Get all possible layouts
	layouts = []
	prevrows = []
	for col in range(1, maxcols, 1):
		nrows = math.ceil(size / col)
		# print(f"nrows: {nrows}")
		if nrows not in prevrows:
			if even == True and not nrows % 2 == 0:
				continue
			layouts.append(Layout(nrows, col))
			prevrows.append(nrows)

	layouts.reverse()
	return layouts
